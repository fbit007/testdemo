package org.seckill.dto;

/**
 * 暴露秒杀接口ＤＴＯ
 * Created by Huaxing on 2016/7/6.
 */
public class Exposer {


    private boolean exposed;
    private String md5;
    private long seckillId;

    private long now;
    private long start;
    private long end;

    public Exposer(boolean exposed,long seckillId, long now, long start,long end) {
        this.exposed = exposed;
        this.seckillId = seckillId;
        this.end = end;
        this.now = now;
        this.start = start;
    }

    public Exposer(boolean exposed, String md5, long seckillId) {

        this.exposed = exposed;
        this.md5 = md5;
        this.seckillId = seckillId;
    }

    @Override
    public String toString() {
        return "Exposer{" +
                "end=" + end +
                ", exposed=" + exposed +
                ", md5='" + md5 + '\'' +
                ", seckillId=" + seckillId +
                ", now=" + now +
                ", start=" + start +
                '}';
    }

    public Exposer(boolean exposed, long seckillId) {
        this.seckillId = seckillId;
        this.exposed = exposed;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public boolean isExposed() {
        return exposed;
    }

    public void setExposed(boolean exposed) {
        this.exposed = exposed;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public long getNow() {
        return now;
    }

    public void setNow(long now) {
        this.now = now;
    }

    public long getSeckillId() {
        return seckillId;
    }

    public void setSeckillId(long seckillId) {
        this.seckillId = seckillId;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }
}
